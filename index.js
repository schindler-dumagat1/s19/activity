// console.log("Hello Hello");

const num = 2;
const getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);

const address = [258, 'Washington Ave NW', "California", 90011];

const [houseNumber, place, state, areaCode] = address;

console.log(`I live at ${houseNumber} ${place}, ${state} ${areaCode}`);

const animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: '1075 kgs',
	measurement: '20 ft 3 in'
}

const {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);

const myNum = [1, 2, 3, 4, 5];

myNum.forEach( (number) => {console.log(number)});

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog('Frankie', 5, 'Miniature Dachshund');
console.log(myDog);
